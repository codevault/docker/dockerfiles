FROM ubuntu:22.04 as build

RUN apt-get -qq update
RUN apt-get -qq install wget -y

ARG JAVA_MAJOR_VERSION=21
ARG JAVA_VERSION=${JAVA_MAJOR_VERSION}

RUN wget -q https://download.oracle.com/java/${JAVA_MAJOR_VERSION}/latest/jdk-${JAVA_VERSION}_linux-x64_bin.tar.gz
RUN tar -xf jdk-${JAVA_VERSION}_linux-x64_bin.tar.gz

FROM ubuntu:22.04 as main

ARG JAVA_MAJOR_VERSION=21
ARG JAVA_VERSION=${JAVA_MAJOR_VERSION}

ENV JAVA_HOME=/usr/local/jdk-${JAVA_VERSION}
ENV PATH=$PATH:$JAVA_HOME/bin

COPY --from=build /jdk-${JAVA_VERSION} /usr/local/jdk-${JAVA_VERSION}

RUN java --version
