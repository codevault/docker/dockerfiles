FROM ubuntu:22.04 as java

RUN apt-get -qq update
RUN apt-get -qq install wget -y

ARG JAVA_MAJOR_VERSION=21
ARG JAVA_VERSION=${JAVA_MAJOR_VERSION}

RUN wget -q https://download.oracle.com/java/${JAVA_MAJOR_VERSION}/latest/jdk-${JAVA_VERSION}_linux-x64_bin.tar.gz
RUN tar -xf jdk-${JAVA_VERSION}_linux-x64_bin.tar.gz

FROM ubuntu:22.04 as mvn

RUN apt-get -qq update
RUN apt-get -qq install wget -y

ARG MAVEN_MAJOR_VERSION=3
ARG MAVEN_MINOR_VERSION=9
ARG MAVEN_PATCH_VERSION=4
ARG MAVEN_VERSION=${MAVEN_MAJOR_VERSION}.${MAVEN_MINOR_VERSION}.${MAVEN_PATCH_VERSION}

RUN wget -q https://dlcdn.apache.org/maven/maven-${MAVEN_MAJOR_VERSION}/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
RUN tar -xf apache-maven-${MAVEN_VERSION}-bin.tar.gz

FROM ubuntu:22.04 as main

ARG JAVA_MAJOR_VERSION=21
ARG JAVA_VERSION=${JAVA_MAJOR_VERSION}

ARG MAVEN_MAJOR_VERSION=3
ARG MAVEN_MINOR_VERSION=9
ARG MAVEN_PATCH_VERSION=4
ARG MAVEN_VERSION=${MAVEN_MAJOR_VERSION}.${MAVEN_MINOR_VERSION}.${MAVEN_PATCH_VERSION}

ENV JAVA_HOME=/usr/local/jdk-${JAVA_VERSION}
ENV M2_HOME=/opt/apache-maven-${MAVEN_VERSION}

ENV PATH=$PATH:$JAVA_HOME/bin:$M2_HOME/bin

COPY --from=java /jdk-${JAVA_VERSION} /usr/local/jdk-${JAVA_VERSION}
COPY --from=mvn /apache-maven-${MAVEN_VERSION} /opt/apache-maven-${MAVEN_VERSION}

RUN java --version
RUN mvn -version
